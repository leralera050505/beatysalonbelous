﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;
using SalonB.HelpClass;

namespace SalonB.Windows
{
    /// <summary>
    /// Логика взаимодействия для CheckWindow.xaml
    /// </summary>
    public partial class CheckWindow : Window
    {
        public CheckWindow()
        {
            InitializeComponent();
            List<Service> services = new List<Service>();
            services = MyContext.Service.ToList();
            LVCheckList.ItemsSource = services;
        }
        
        private void AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddEditService addEditService = new AddEditService();
            addEditService.Show();
            this.Close();
        }

        private void btnBasket_Click(object sender, RoutedEventArgs e)
        {
            BasketWindow basketWindow = new BasketWindow();
            basketWindow.ShowDialog();
            this.Hide();
        }

        private void BtnAddToService_Click(object sender, RoutedEventArgs e)
        {
            bool seek = true;
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Service selectedService = button.DataContext as DB.Service;
            if (selectedService != null)
            {
                for (int i = 0; i < HelpClass.BasketClass.Services.Count; i++)
                {
                    if (HelpClass.BasketClass.Services[i] == selectedService)
                    {
                        HelpClass.BasketClass.Services[i].Quantity++;
                        seek = false;
                    }
                }
                if (seek)
                {
                    selectedService.Quantity = 1;
                    HelpClass.BasketClass.Services.Add(selectedService);
                }
            }
        }
    }
}
