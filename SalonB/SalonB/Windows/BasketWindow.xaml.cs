﻿using SalonB.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;
using SalonB.HelpClass;

namespace SalonB.Windows
{
    /// <summary>
    /// Логика взаимодействия для BasketWindow.xaml
    /// </summary>
    public partial class BasketWindow : Window
    {
        public BasketWindow()
        {
            InitializeComponent();
            GetListService();
        }

        private void GetListService()
        {
            ObservableCollection<Service> services = new ObservableCollection<Service>(BasketClass.Services);
            LVBasket.ItemsSource= services;
            GetPrice(services);

        }
        private void GetPrice(ObservableCollection<Service> services)
        {
            DateTime date1 = new DateTime(2023, 11, 07);
            DateTime date2 = new DateTime(2023, 11, 25);
            decimal discount = 0;
            decimal price = 0;
            if (DateTime.Now >= date1 && (DateTime.Now <= date2))
            {
                foreach (var item in BasketClass.Services)
                {
                    item.GetPrice = Convert.ToDecimal(((item.Cost-(item.Cost * 0.2m)) * item.Quantity)); //cскидка на каждый продукт
                    discount = item.GetPrice;
                }
                
            }
                foreach (var item in BasketClass.Services)
                {
                    price += Convert.ToDecimal(item.Cost * item.Quantity);
                }
                price = Math.Round(price, 2);
                discount= Math.Round(discount, 2);

                tbAllCost.Text = price.ToString();
                tbDiccountCost.Text = discount.ToString();
            
        }
        private void BtnAddToCart_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            DB.Service selectedService = button.DataContext as DB.Service;
            if (selectedService != null)
            {
                selectedService.Quantity++;
                int o = BasketClass.Services.IndexOf(selectedService);
                BasketClass.Services.Remove(selectedService);
                BasketClass.Services.Insert(o, selectedService);
            }
            GetListService();
        }

        private void BtnRemoveToCart_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            DB.Service selectedService = button.DataContext as DB.Service;

            if (selectedService != null)
            {
                if (selectedService.Quantity == 1 || selectedService.Quantity == 0)
                {
                    HelpClass.BasketClass.Services.Remove(selectedService);
                }
                else
                {
                    selectedService.Quantity--;
                    int o = HelpClass.BasketClass.Services.IndexOf(selectedService);
                    HelpClass.BasketClass.Services.Remove(selectedService);
                    HelpClass.BasketClass.Services.Insert(o, selectedService);
              
                }
            }
            GetListService();
        }

        private void AddService_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<DB.Service> services = new ObservableCollection<DB.Service>(HelpClass.BasketClass.Services);
            if (services.Count() == 0)
            {
                MessageBox.Show("Корзина пуста");
            }
            else
            {
                DB.Check check = new DB.Check();
                check.IdClient = 1;
                check.Date = DateTime.Now;
                if (check != null)
                {
                    HelpClass.EFClass.MyContext.Check.Add(check);
                    HelpClass.EFClass.MyContext.SaveChanges();
                }

                foreach (var item in HelpClass.BasketClass.Services)
                {
                    DB.CheckService checkService = new DB.CheckService();
                    checkService.IdCheckService = MyContext.CheckService.ToList().LastOrDefault().IdCheckService + 1;
                    checkService.IdService = item.IdService;
                    checkService.IdCheck = MyContext.Check.ToList().LastOrDefault().IdCheck;
                    checkService.IdWorker = MyContext.Worker.ToList().LastOrDefault().IdWorker;

                    MyContext.CheckService.Add(checkService);
                    MyContext.SaveChanges();
                }
                HelpClass.BasketClass.Services = new List<DB.Service>();
                MessageBox.Show("Продукты успешно добавлены");
            }
            Close();
        }

      
        
    }
}
