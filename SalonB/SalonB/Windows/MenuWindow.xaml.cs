﻿using SalonB.HelpClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SalonB.HelpClass.Navigation;
using SalonB.Pages;


namespace SalonB.Windows
{
    /// <summary>
    /// Логика взаимодействия для MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            InitializeComponent();
            Navigation.frame = frameName;
            Navigation.frame.Navigate(new Page());
        }

        private void btnClient_Click(object sender, RoutedEventArgs e)
        {
            Navigation.frame.Navigate(new ClientPage());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Navigation.frame.Navigate(new ServicePage());
        }

        private void btnProduct_Click(object sender, RoutedEventArgs e)
        {
            Navigation.frame.Navigate(new ProductPage());
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            CheckWindow checkWindow = new CheckWindow();
            checkWindow.Show();
            this.Hide();
        }
    }
}
