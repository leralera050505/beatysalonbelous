﻿using SalonB.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;
using static Microsoft.Win32.OpenFileDialog;
using Microsoft.Win32;

namespace SalonB.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddPRoductWindow.xaml
    /// </summary>
    public partial class AddPRoductWindow : Window
    {

        public AddPRoductWindow()
        {
            InitializeComponent();
            cmbManufacturer.ItemsSource = MyContext.Manufacturer.ToList();
            cmbManufacturer.SelectedIndex = 0;
            cmbManufacturer.DisplayMemberPath = "NameManufacturer";

            cmbCategoryProduct.ItemsSource = MyContext.CategoryProduct.ToList();
            cmbCategoryProduct.SelectedIndex = 0;
            cmbCategoryProduct.DisplayMemberPath = "NameCategory";
        }

       
        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            //    CategoryProduct categoryProduct = new CategoryProduct();
            //    categoryProduct.NameCategory = (cmbCategoryProduct.SelectedItem as DB.CategoryProduct).NameCategory;
            //    MyContext.CategoryProduct.Add(categoryProduct);
            //    MyContext.SaveChanges();

                Manufacturer manufacturer = new Manufacturer();
                manufacturer.NameManufacturer = (cmbManufacturer.SelectedItem as DB.Manufacturer).NameManufacturer;
                MyContext.Manufacturer.Add(manufacturer);
                MyContext.SaveChanges();
                
                Product product = new Product();
                product.NameProduct = txtNameProduct.Text;
                product.Cost = Convert.ToDecimal(txtCost.Text);
                product.Description= txtDescription.Text;
                product.IdCategoryProduct = (cmbCategoryProduct.SelectedItem as DB.CategoryProduct).IdCategoryProduct;
                MyContext.Product.Add(product);
                MyContext.SaveChanges();

                MessageBox.Show("Добавление прошло успешно");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
    }
}
