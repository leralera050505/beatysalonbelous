﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace SalonB.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddClientWindow.xaml
    /// </summary>
    public partial class AddClientWindow : Window
    {
        public AddClientWindow()
        {
            InitializeComponent();
            CmbTaq.ItemsSource = MyContext.Client.ToList();
            CmbTaq.DisplayMemberPath = "NameTag";
            CmbTaq.SelectedIndex = 0;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Client client = new Client();

                CmbTaq.Text = client.ClientTag.NameTag.ToString();
                if (client.ClientTag.NameTag == "не постоянный")
                {
                    tbFirstNAme.Text = null;
                    tbFirstNAme.Text = null;
                    TBPatronymic.Text = null;
                    TbPhone.Text = null;
                    
                }
                else
                {
                    client.FirsName = tbFirstNAme.Text;
                    client.LastName = tbFirstNAme.Text;
                    client.Patronymic = TBPatronymic.Text;
                    client.Phone = TbPhone.Text;
                }
                MyContext.Client.Add(client);
                MyContext.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
