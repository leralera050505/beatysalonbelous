﻿using SalonB.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace SalonB.Pages
{
    /// <summary>
    /// Логика взаимодействия для ServicePage.xaml
    /// </summary>
    public partial class ServicePage : Page
    {
        List<Service> services = new List<Service>();
        List<string> sortlist = new List<string>()
        {
            "По наименовании",
            "По цене",
            "По длительности"
        };
        public ServicePage()
        {
            InitializeComponent();
            CmbSort.ItemsSource = sortlist;
            CmbSort.SelectedIndex = 0;
            GetListService();
            listviewService.ItemsSource = MyContext.Service.ToList();
        }
        private void GetListService()
        {
            services = MyContext.Service.ToList();
            services = services.
                Where(i => i.NameService.Contains(TxbSearch.Text)
                || i.Cost.ToString().Contains(TxbSearch.Text)
                || i.DurationMinute.ToString().Contains(TxbSearch.Text)
                || i.Description.Contains(TxbSearch.Text)
                || i.CategoryService.NameCategory.Contains(TxbSearch.Text)
                ).ToList();

            switch (CmbSort.SelectedIndex)
            {
                case 0:
                    services = services.OrderBy(i => i.NameService).ToList();
                    break;

                case 1:
                    services = services.OrderBy(i => i.Cost).ToList();
                    break;

                case 2:
                    services = services.OrderBy(i => i.DurationMinute).ToList();
                    break;
                default:
                    break;
            }
            listviewService.ItemsSource = services;
        }
        private void CmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetListService();
        }

        private void TxbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            GetListService();
        }
    }
}
