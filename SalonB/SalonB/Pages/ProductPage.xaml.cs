﻿using SalonB.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace SalonB.Pages
{
    /// <summary>
    /// Логика взаимодействия для ProductPage.xaml
    /// </summary>
    public partial class ProductPage : Page
    {
        List<Product> products = new List<Product>();
        List<string> sortlist = new List<string>()
        {
            "По наименовании",
            "По цене",
            "По поставщику",
            "По категории"
        };
        public ProductPage()
        {
            InitializeComponent();
            CmbSort.ItemsSource = sortlist;
            CmbSort.SelectedIndex = 0;
            GetListProduct();
            listviewProduct.ItemsSource = MyContext.Product.ToList();
        }
        private void GetListProduct()
        {
            products = MyContext.Product.ToList();
            products = products.
                Where(i => i.NameProduct.Contains(TxbSearch.Text)
                || i.Cost.ToString().Contains(TxbSearch.Text)
                || i.Description.Contains(TxbSearch.Text)
                || i.Manufacturer.NameManufacturer.Contains(TxbSearch.Text)
                || i.CategoryProduct.NameCategory.Contains(TxbSearch.Text)
                ).ToList();

            switch (CmbSort.SelectedIndex)
            {
                case 0:
                    products = products.OrderBy(i => i.NameProduct).ToList();
                    break;

                case 1:
                    products = products.OrderBy(i => i.Cost).ToList();
                    break;

                case 2:
                    products = products.OrderBy(i => i.Manufacturer.NameManufacturer).ToList();
                    break;

                case 3:
                    products = products.OrderBy(i => i.CategoryProduct.NameCategory).ToList();
                    break;
                default:
                    break;
            }
            listviewProduct.ItemsSource = products;
        }
        private void TxbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            GetListProduct();
        }

        private void CmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetListProduct();
        }

        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddPRoductWindow addPRoductWindow = new AddPRoductWindow();
            addPRoductWindow.ShowDialog();
        }
    }
}
