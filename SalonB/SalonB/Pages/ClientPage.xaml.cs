﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static SalonB.HelpClass.EFClass;
using SalonB.Windows;
using SalonB.DB;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace SalonB.Pages
{
    /// <summary>
    /// Логика взаимодействия для ClientPage.xaml
    /// </summary>
    public partial class ClientPage : Page
    {
        List<Client> clients = new List<Client>();
        List<string> sortlist = new List<string>()
        {
            "По имени",
            "По фамилии",
            "По отчеству",
            "По телефону",
            "По тегу",
        };
        public ClientPage()
        {
            InitializeComponent();
            CmbSort.ItemsSource = sortlist;
            CmbSort.SelectedIndex = 0;
            GetListClient();
            listviewClient.ItemsSource = MyContext.Client.ToList();
        }
        private void GetListClient()
        {
            clients = MyContext.Client.ToList();
            clients = clients.
                Where (i => i.FirsName.Contains(TxbSearch.Text)
                || i.LastName.Contains(TxbSearch.Text)
                || i.Patronymic.Contains(TxbSearch.Text)
                || i.Phone.Contains(TxbSearch.Text)
                || i.ClientTag.NameTag.Contains(TxbSearch.Text)
                ).ToList();

            switch (CmbSort.SelectedIndex)
            {
                case 0:
                    clients = clients.OrderBy(i => i.FirsName).ToList();
                    break;

                case 1:
                    clients = clients.OrderBy(i => i.LastName).ToList();
                    break;

                case 2:
                    clients = clients.OrderBy(i => i.Patronymic).ToList();
                    break;

                case 3:
                    clients = clients.OrderBy(i => i.Phone).ToList();
                    break;
                case 4:
                    clients = clients.OrderBy(i => i.ClientTag.NameTag).ToList();
                    break;
                default:
                    break;
            }
            listviewClient.ItemsSource = clients;
        }
        private void TxbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            GetListClient();
        }

        private void CmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetListClient();
        }


        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddClientWindow addClientWindow = new AddClientWindow();
            addClientWindow.Show();
        }
    }
}
