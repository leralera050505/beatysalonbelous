"# BeatySalonBelous" 
Описание предметной области.
Система “SalonBeaty” предназначена для салона красоты, а именно для администратора заведения, чтобы управлять записями на прием, отслеживать постоянных клиентов, Добавлять или удалять записи из базы данных, например, можно создать новую услугу или продукт. Также можно сделать заказ для определенного клиента. Ссылка на ТЗ https://gitlab.com/leralera050505/beatysalonbelous/-/blob/main/TZ.docx?ref_type=heads
![image](бд.png)
Основные сущности: Client, Worker, Service, Check, Product


usecase диграмма:
![image](usecase.png)
Последовательная диаграмма:
![image](Последовательная_диаграмма.png)

Авторизация:
![image](1.png)
Клиенты:
![image](2.png)
Услуги:
![image](3.png)
Продукты:
![image](4.png)
Добавление в корзину:
![image](5.png)
Корзина:
![image](6.png)
![image](7.png)





